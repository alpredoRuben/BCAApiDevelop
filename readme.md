# REST API IRS DOCUMENTATION

# A. IRS OPERATOR
### **1. REQUEST ALL DATA OPERATOR**
***Format Request All Data Operator***
> 
    url     : https://www.pay-inm.co.id/inm_irs/request/all/data/operator, 
    method  : GET,

***HTTP Response Successfully***
```
    {
        "status"  : true,
        "messages": "Request Product Successfully. Record Available",
        "optional": [
            {
                "idoperator": "6",
                "namaoperator": "Flexy",
                "flagsekat": "0",
                "mindigit": "10",
                "maxdigit": "12"
            },
            {
                "idoperator": "8",
                "namaoperator": "XL",
                "flagsekat": "0",
                "mindigit": "10",
                "maxdigit": "12"
            },
            ... dst
        ]
    }
```

***HTTP Response Error Database Messages***
```
    {
        "status"  : false,
        "messages": "Error To Get Record Data In Database IRS",
        "optional": null
    }
```

***HTTP Response Empty***
```
    {
        "status"  : false,
        "messages": "No Record IRS Data Operator Active",
        "optional": null
    }
```

### **2. REQUEST DATA OPERATOR BY ID**
***Format Request Data Operator By Id***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/data/operator/by/id, 
    method : POST,
    input  : {
        "operator_id": [68, 24] //array data one dimention
    }

***HTTP Response Successfully***
```
    {
        "status": true,
        "messages": "Request Operator Successfully. Record Available",
        "optional": [
            {
                "idoperator": "24",
                "namaoperator": "Axis",
                "flagsekat": "0",
                "mindigit": "10",
                "maxdigit": "12"
            },
            {
                "idoperator": "68",
                "namaoperator": "TELKOMSEL",
                "flagsekat": "0",
                "mindigit": "10",
                "maxdigit": "12"
            }
        ]
    }
```

***HTTP Response Error Database Messages***
```
    {
        "status"  : false,
        "messages": "Error To Get Record Data In Database IRS",
        "optional": null
    }
```

***HTTP Response Empty***
```
    {
        "status"  : false,
        "messages": "No Record IRS Data Operator Active",
        "optional": null
    }
```


# B. IRS PRODUCT

### **1. REQUEST DATA PRODUCT PULSA BY PHONE NUMBER**
***Format Request Data Product Pulsa By Phone Number***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/data/product/pulsa/by/phone_number/(:any), 
    method : GET,
    (:any) : phone number

***HTTP Response Successfully***
```
    {
        "status": true,
        "messages": "Nama Operator TELKOMSEL",
        "optional": [
            {
                "idterminal": "427",
                "idoperator": "68",
                "namaproduk": "SIMPATI 100.000 (MURAH)",
                "kodeproduk": "BANK100",
                "nominal": "100",
                "hargabeli": "93550",
                "isgangguan": "0",
                "isstokkosong": "0"
            },
            {
                "idterminal": "427",
                "idoperator": "68",
                "namaproduk": "SIMPATI 20.000 (MURAH)",
                "kodeproduk": "BANK20",
                "nominal": "20",
                "hargabeli": "19650",
                "isgangguan": "0",
                "isstokkosong": "0"
            },
            ... dst
        ]
    }
```

***HTTP Response Failed***
```
    {
        "status"  : false,
        "messages": "Nomor Pelanggan {nomor telepon] Tidak Valid",
        "optional": null
    }
```



### **2. REQUEST DATA PRODUCT PPOB BY TYPE PPOB**
***Format Request Data Product PPOB By Type PPOB***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/data/product/ppob/by/type_ppob/(:any), 
    method : GET,
    (:any) : type ppob ["pdam", "finance", "bpjs", "hp_pasca", "other"]

***HTTP Response Successfully***
```
    {
        "status": true,
        "messages": [
            {
                "product_id": "74,PDAMATR",
                "product_name": "PDAM AERTRA",
                "product_inf": "0"
            },
            {
                "product_id": "74,PDAMBANJAR",
                "product_name": "PDAM BANJAR MASIN",
                "product_inf": "0"
            },
            ... dst
        ],
        "optional": null
    }
```

***HTTP Response Failed***
```
    {
        "status"  : false,
        "messages": "No Record Data Product {Product Name}",
        "optional": null
    }
```


### **3. REQUEST DATA PRODUCT ACTIVE**
***Format Request Data Product Active***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/all/data/product/active, 
    method : GET

***HTTP Response Successfully***
```
    {
        "status": true,
        "messages": "Request Product Successfully. Record Available",
        "optional": [
            {
                "idterminal": "388",
                "idoperator": "109",
                "namaproduk": "BRONET 2GB 24JAM,60HR",
                "kodeproduk": "24BRO27",
                "nominal": "25",
                "hargabeli": "25300",
                "isgangguan": "0",
                "isstokkosong": "0"
            },
            {
                "idterminal": "388",
                "idoperator": "109",
                "namaproduk": "BRONET 3GB 24JAM,60HR",
                "kodeproduk": "24BRO36",
                "nominal": "40",
                "hargabeli": "33300",
                "isgangguan": "0",
                "isstokkosong": "0"
            },
            ... dst
        ]
    }
```

***HTTP Response Failed***
```
    {
        "status"  : false,
        "messages": "No Record IRS Data Product Active",
        "optional": null
    }
```

### **4. REQUEST DATA PRODUCT ACTIVE BY OPERATOR ID**
***Format Request Data Product Active By Operator Id***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/data/product/active/by/operator_id, 
    method : POST,
    input  : {
        "operator_id" : [68, 24] //array one dimention
    }

***HTTP Response Successfully***
```
    {
        "status": true,
        "messages": "Request Product Successfully. Record Available",
        "optional": [
            {
                "idterminal": "388",
                "idoperator": "24",
                "namaproduk": "AXIS 10.000",
                "kodeproduk": "AX10",
                "nominal": "10",
                "hargabeli": "10400",
                "isgangguan": "0",
                "isstokkosong": "0"
            },
            {
                "idterminal": "388",
                "idoperator": "24",
                "namaproduk": "AXIS 100.000",
                "kodeproduk": "AX100",
                "nominal": "100",
                "hargabeli": "98000",
                "isgangguan": "0",
                "isstokkosong": "0"
            },
            ... dst
        ]
    }
```

***HTTP Response Error Database Messages***
```
    {
        "status"  : false,
        "messages": "Error To Get Record Data In Database IRS",
        "optional": null
    }
```

***HTTP Response Empty***
```
    {
        "status"  : false,
        "messages": "No Record IRS Data Operator Active",
        "optional": null
    }
```



# C. IRS STATUS TRANSACTION

### **1. REQUEST STATUS PAY TRANSACTION PULSA**
***Format Request Status Pay Transaction Pulsa***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/status/pay/transaction/pulsa, 
    method : POST,
    input  : {
        "customer_number" : "085206785139", // Nomor Pelanggan
        "product_code"    : "S5", // Kode Produk Dari List Produk
        "transaction_id"  : "No Transaksi Setelah Request Payment Transaksi Berhasil"
    }

***HTTP Response Successfully***
```
    {
        "success": true,
        "msg": "TRX TN5.085206785139 BERHASIL,HARGA: 5.349 SN: 8032217225081412500. SISA SALDO: 1.398.363 - 5.349 = 1.393.014 @3/22/2018 5:23:00 PM  REFF#1809"
    }
```

***HTTP Response Failed***
```
    {
        "success": false,
        "clientid": null,
        "tujuan": "",
        "msg": "NOMOR TUJUAN TIDAK BOLEH KOSONG"
    }
```

***HTTP Response Transaction Failed***
```
    {
        "success": false,
        "clientid": "1807",
        "tujuan": "085206785139",
        "msg": "TRX TN5 KE 085206785139 GAGAL. MOHON DIPERIKSA KEMBALI NO TUJUAN SEBELUM DI ULANG. SALDO: RP 1.398.363. 3/22/2018 5:09:48 PM  REFF#1807"
    }
```


### **2. REQUEST STATUS TRANSACTION BY TRANSACTION ID**
***Format Request Status Transaction By Transaction Id***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/status/transaction/by/trx_id, 
    method : POST,
    input  : {
        "transaction_id"  => "No Transaksi Setelah Request Payment Transaksi Berhasil"
    }

***HTTP Response Successfully***
```
    {
        "status": true,
        "messages": {
            "status_trx": "2",
            "description": "{SUCCESS:FALSE,CLIENTID:1807,TUJUAN: 085206785139,MSG:TRX TN5 KE 085206785139 GAGAL. MOHON DIPERIKSA KEMBALI NO TUJUAN SEBELUM DI ULANG. SALDO: RP 1.398.363. 3/22/2018 5:09:48 PM  REFF#1807}",
            "inbox_code": "032205081628430",
            "sn": ""
        },
        "optional": null
    }
```

***HTTP Response Error Database***
```
    {
        "status": false,
        "messages": "Error To Get Record Data In Database Irs",
        "optional": null
    }
```

***HTTP Response Failed Record***
```
    {
        "status": false,
        "messages": "No Record IRS Data Product Active",
        "optional": null
    }
```


# D. IRS TRANSACTION

### **1. REQUEST TOP UP TRANSACTION PULSA**
***Format Request Top Up Transasction Pulsa***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/pay/transaction/pulsa, 
    method : POST,
    input  : {
        "customer_number" : "085206785139", // Nomor Pelanggan
        "product_code"    : "S5", // Kode Produk Dari Database Produk IRS
    }

***HTTP Response Successfully***
```
    {
        "success" : true,
        "messages": "Request S5 ke 085206785139 under proses..",
        "optional": "1807"
    }
```

***HTTP Response Failed***
```
    {
        "success" : false,
        "messages": " Kode S5 ke 085760806067 Nomor tujuan dan produk tidak sesuai"
        "optional": "1807"
    }
```

### **2. REQUEST TOP UP TRANSACTION PLN**
***Format Request Top Up Transasction PLN***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/pay/transaction/pulsa, 
    method : POST,
    input  : {
        "customer_number" : "22114970100", // Nomor Pelanggan
        "product_code"    : "PL20", // Kode Produk Dari Database Produk IRS
    }

***HTTP Response Successfully***
```
    {
        "success" : true,
        "messages": [{
            "no_token"       : 2651-6611-5351-7532-2377,
            "nama_pelanggan" : LIE SUDIANTO,
            "gol_reff"       : R2,
            "no_tarif"       : 4400,
            "nilai_kwh"      : 12.70KWH    
        }],
        "optional": "032105521995840"
    }
```

***HTTP Response Failed***
```
    {
        "success" : false,
        "messages": "Kode PL20 ke 22114970100 Tidak Sesuai"
        "optional": "1806"
    }
```

### **2. REQUEST INQUIRY TRANSACTION PPOB**
***Format Request Transasction PPOB***
>
    url    : https://www.pay-inm.co.id/inm_irs/request/inq/transaction/ppob, 
    method : POST,
    input  : {
        "customer_number" : "", // Nomor Pelanggan
        "product_code"    : "", // Kode Produk Dari Database Produk IRS
    }

***HTTP Response Inquiry***
```
    {
        "success" : true,
        "messages": "Response Inquiry From IRS Server",
        "optional": "032105521995840"
    }
```

*Format PPOB Di IRS Untuk Sementara Tidak Dapat Digunakan Melalui Transaksi IP*