'use strict'
var dataPulsa = null;

var getCurrencyFormat = function(bilangan) {

    var	reverse = bilangan.toString().split('').reverse().join(''),
	rupiah 	= reverse.match(/\d{1,3}/g);
	rupiah	= rupiah.join('.').split('').reverse().join('');
    return rupiah;
}

var AlertSuccess = function(messages){
    $.alert({
        title: "SUCCESS INFO",
        content: messages,
        type: "dark",
        theme : "modern",
        columnClass: "col-md-5",
        typeAnimated: true
    });
}


var AlertError = function(messages){
    $.alert({
        title: "ERROR INFO",
        content: messages,
        type: "red",
        theme : "modern",
        columnClass: "col-md-5",
        typeAnimated: true
    });
}

$(document).ready(function () {
   
    var TextNamaProvider    = $("#txtNamaProvider");
    var TextBoxNoPelanggan  = $("#txtNoPelanggan");
    var TextBoxTotalBayar   = $("#txtTotalBayar");
    var LblDescriptingPulsa = $("#lblDescriptPulsa");
    var ComboBoxProdukPulsa = $("select[name=selectProdukPulsa]");
    var ButtonPaymentPulsa  = $("#btnBayarPulsa");
   
    $(TextBoxNoPelanggan).keypress(function (e) { 
        if(e.keyCode == 13){
            var input = $(this).val();

            if(input.length > 4)
            {
                var strUrl = window.site_url + 'get/irs_service/operator/pulsa/by/phone/number/' + input;

                $.getJSON(strUrl, function (response) {
                    var messages = response.messages;
                    var status = response.status;
                    
                    console.log(response);
                    //TextNamaProvider.val()
                });

            }

        }
    });

    // $(TextBoxNoPelanggan).keypress(function (e) { 
    //     if(e.keyCode == 13){
    //         var input = $(this).val();
            
    //         if(input.length > 4){
    //             var strUrl = window.site_url + "data/produk/pulsa/nomor/" + input;
                
    //             $.getJSON(strUrl, function(response) {
        
    //                 var status   = response.status,
    //                     messages = response.messages,
    //                     optional = response.optional;
                    
    //                 if(status != false){
    //                     dataPulsa = optional;
    //                     ComboBoxProdukPulsa.empty();
        
    //                     $.each(optional, function( key, val ) {
    //                         var exval = val.idterminal + "," + val.kodeproduk ;
    //                         $('<option>').val(exval).text(val.namaproduk).appendTo(ComboBoxProdukPulsa);
    //                         //console.log("Record Ke-" + key + ": " + val.idterminal + ", " + val.kodeproduk + ", " + val.namaproduk);
    //                     });
        
    //                     LblDescriptingPulsa.text(messages);
    //                 }
    //                 else{
    //                     AlertError(messages);
    //                 }
    //             });
    //         }
    //         else if(input.length > 0 && input.length <= 4){
    //             AlertError("Nomor Pelanggan Tidak Valid");
    //         }
    //         else{
    //             AlertError("Silahkan Isi No Pelanggan");
    //         }

    //         return false;
    //     }
    // });

    $(ComboBoxProdukPulsa).change(function (e) { 
        e.preventDefault();
        
        var selection = ComboBoxProdukPulsa.val();
        var splits = selection.split(",");
        var hargaJual = 0;
        if(dataPulsa.length > 0)
        {
            $.each(dataPulsa, function( key, val ) {
                if(splits[0] == val.idterminal && splits[1] == val.kodeproduk){
                    hargaJual = parseInt(val.hargabeli) + 100;
                }
            });
            
            hargaJual = getCurrencyFormat(hargaJual);
            TextBoxTotalBayar.val("Rp. " + hargaJual);
        }
        else{
            console.log("tidak ada");
        }
    });
    
    $(ButtonPaymentPulsa).click(function (e) { 
        e.preventDefault();
        var NoPelanggan = TextBoxNoPelanggan.val();
        var strUrl = window.site_url + "pay/transaction/produk/pulsa";

        if(!NoPelanggan){
            AlertError("Silahkan Isi No Pelanggan");
        }
        else{
            var splits = ComboBoxProdukPulsa.val().split(",");
            var dataFormPulsa = {
                no_pelanggan: NoPelanggan,
                terminal_id : splits[0],
                kode_produk : splits[1],
                total_bayar : TextBoxTotalBayar.val()
            };

            axios.post(strUrl, dataFormPulsa).then(function (response) {
                var messages = response.data.messages,
                    trans_id = response.data.optional,
                    status   = response.data.status;

                if(status != false)
                {   
                    console.log(trans_id);

                    AlertSuccess(messages + " Silahkan Menunggu");

                    var strUrlCheck = window.site_url + "check/pay/transaction/pulsa";

                    setTimeout(function(){ 
                        axios.post(strUrlCheck, {
                            no_pelanggan:dataFormPulsa.no_pelanggan, 
                            kode_produk:dataFormPulsa.kode_produk, 
                            transaksi_id: trans_id 
                        }).then(function (state_response) {
                            console.log(state_response);
                            var res_status = state_response.data.success;
                            var res_messages = state_response.data.msg;

                            if(res_status != false)
                                AlertSuccess(res_messages);
                            else
                                AlertError(res_messages);

                        }).catch(function (state_error) {
                            console.log(state_error);
                        });
                     }, 15000);
                }
                else{
                    AlertError(messages);
                }

            }).catch(function (error) {
                console.log(error);
            });
    
        }


    });





});