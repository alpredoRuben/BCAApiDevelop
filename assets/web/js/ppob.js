'use strict'
var AlertError = function(messages){
    $.alert({
        title: "ERROR INFO",
        content: messages,
        type: "red",
        theme : "modern",
        columnClass: "col-md-5",
        typeAnimated: true
    });
}

$(document).ready(function () {
    
    var TextBoxNoPelanggan = $("#txtNoPelanggan");
    var ComboBoxTypePPOB   = $("select[name=selectJenisPPOB]");
    var ComboBoxProdukPPOB = $("select[name=selectProdukPPOB]");
    var ButtonCheckBilling = $("#btnCheckTagihan");


    $(ComboBoxTypePPOB).change(function (e) { 
        e.preventDefault();
        var type_ppob = $(this).val();
        var strUrl = window.site_url + "data/produk/ppob/" + type_ppob;
        
        if(type_ppob != '-'){
            $.getJSON(strUrl, function(response) {
                console.log(response);
                var status = response.status;
                var messages = response.messages;

                if(status == true){
                    ComboBoxProdukPPOB.empty();
                    var strVal = "";
                    $.each(messages, function( key, val ) {
                        if(val.product_inf == "0")
                            strVal = val.product_name + ", AKTIF";
                        else
                            strVal = val.product_name + ", NONAKTIF";

                        $('<option>').val(val.product_id).text(strVal).appendTo(ComboBoxProdukPPOB);
                    });
                }
                else{
                    AlertError(messages);
                }
            });
        }
        
    });

    $(ButtonCheckBilling).click(function (e) { 
        e.preventDefault();
        var produk_ppob = ComboBoxProdukPPOB.val().split(",");
        var customer_no = TextBoxNoPelanggan.val();
        var strUrl = window.site_url + "request/inquiry/produk/ppob";

        var dataFormPPOB = {
            no_pelanggan : customer_no,
            kode_produk  : produk_ppob[1]
        };

        axios.post(strUrl, dataFormPPOB).then(function (response) {
            console.log(response);
        }).catch(function (error) {
            console.log(error);
        });
        

    });

});