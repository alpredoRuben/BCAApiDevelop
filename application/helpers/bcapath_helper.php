<?php defined('BASEPATH') OR exit('No direct script access allowed');



if(!function_exists('PathBalanceInformation'))
{
	function PathBalanceInformation($corporate_id, $account_number)
	{
		return '/banking/v3/corporates/'.$corporate_id.'/accounts/'.$account_number;
	}
}

if(!function_exists('PathAccountBalance'))
{
	function PathAccountBalance($corporate_id, $account_number, $start_date, $end_date)
	{
		return '/banking/v3/corporates/'.$corporate_id.'/accounts/'.$account_number.'/statements?StartDate='.$start_date.'&EndDate='.$end_date;
	}
}