<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class BCAController extends BCAConfig 
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function getMutasiBCA()
    {
        $input = json_decode(file_get_contents('php://input'), true);
        
        $start_date = $this->changeFormatDate($input['start_date']);
        $end_date   = $this->changeFormatDate($input['end_date']);
        $response = $this->business_banking->getAccountBalanceInfo($start_date, $end_date);

        if(!empty($response) && count($response) > 0)
        {
            $mutasi = array();

            if(isset($response['ErrorCode']))
            {
                $mutasi['kode_error'] = $response['ErrorCode'];
                $mutasi['pesan_error'] = $response['ErrorMessage']['Indonesian'];

                $this->setResponse(false, $mutasi);
            }
            else
            {
                $mutasi = $this->setArrayMutasi($response);
                $this->setResponse(true, $mutasi);
            }

        }
        else
            $this->setResponse(false, 'No Record Data Mutasi');

    }


    public function getFetchDataTableMutasi()
    {
        $start_date = $this->changeFormatDate($_POST['start_date']);
        $end_date   = $this->changeFormatDate($_POST['end_date']);
        $response = $this->business_banking->getAccountBalanceInfo($start_date, $end_date);
        
        if(!empty($response) && count($response) > 0 && is_array($response))
        {
            $mutasi = array();

            if(isset($response['ErrorCode']))
            {
                $mutasi['kode_error']  = $response['ErrorCode'];
                $mutasi['pesan_error'] = $response['ErrorMessage']['Indonesian'];

                $output = array(
                    "draw"            => $_POST['draw'],
                    "recordsTotal"    => count($fetchs),
                    "recordsFiltered" => count($fetchs),
                    "data"            => $mutasi['pesan_error'],
                );
                echo json_encode($output);
            }
            else
            {
                $mutasi = $this->setArrayMutasi($response);

                if(count($mutasi['data_mutasi']) > 0)
                {
                    $fetchs = array();

                    $i=0; 
                    foreach ($mutasi['data_mutasi'] as $v) {
                        $fetchs[$i] = array(
                            $i+1,
                            $v["tanggal_transaksi"],
                            $v["kode_cabang"],
                            $v["jenis_transaksi"],
                            $v["jumlah_transaksi"],
                            $v["nama_transaksi"],
                            $v["deskripsi_transaksi"]
                        );
                        $i++;
                    }

                    $output = array(
                        "draw"            => $_POST['draw'],
                        "recordsTotal"    => count($fetchs),
                        "recordsFiltered" => count($fetchs),
                        "data"            => $fetchs,
                    );

                    echo json_encode($output);
                }
                else
                    echo json_encode(array());

                }
            }
        else
            echo json_encode('No Record Data');

    }

    
    
}

/* End of file BCAController.php */
