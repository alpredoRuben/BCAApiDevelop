<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class BussinessBanking extends BCAServices 
{
    private $cid = null;
    private $acn = null;

    public function __construct()
    {
        parent::__construct();
        $this->cid = $this->getCorporateId();
        $this->acn = $this->getAccountNumber();
    }
    
    public function getBalanceInfo()
    {
        $this->setAccessToken();
        $path = PathBalanceInformation($this->cid, $this->acn);
        $method = 'GET';

        $this->setUtilitySignature($path, $method);
        $runtime_header = $this->getRuntimeHeader();
        $response = $this->BCAGetRequestService($path, $runtime_header);   
        return $response;
    }
    
    public function getAccountBalanceInfo($start_date, $end_date)
    {
        $this->setAccessToken();
        $path = PathAccountBalance($this->cid, $this->acn, $start_date, $end_date);
        $method = 'GET';
        $data = array();

        $this->setUtilitySignature($path, $method, $data);
        $runtime_header = $this->getRuntimeHeader();
        $response = $this->BCAGetRequestService($path, $runtime_header);   
        return $response;
    }


	
}
