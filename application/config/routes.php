<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['request/data/mutasi/bank/bca'] = 'BCAController/getMutasiBCA';

$route['test/dev/fetch/mutasi'] = 'BCAController/getFetchDataTableMutasi';

/** Default Route Page */
$route['default_controller']    = "ViewDevelopTester/viewMutasi" ;
$route['404_override'] 	        = '';
$route['translate_uri_dashes'] 	= TRUE;
