<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dev BCA</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.8/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <style>
        body
        {
            margin:0;
            padding:0;
            background-color:#f1f1f1;
        }
        .box
        {
            width:1270px;
            padding:20px;
            background-color:#fff;
            border:1px solid #ccc;
            border-radius:5px;
            margin-top:25px;
        }
        .mylist{
            list-style-type:none;
        }
    </style>

</head>
<body>

<div class="container box">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="text-center text-info">API SERVICE MUTASI</h4>
            <p>Format Entry Tanggal Mulai Atau Akhir : MM-DD-YYYY</p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <form class="form-inline" action="/action_page.php">
                <div class="form-group">
                    <label for="tgl_mulai">Tanggal Mulai</label>
                    <input type="text" id="tgl_mulai" name="tgl_mulai" class="form-control" value="09/01/2016"/>
                </div>
                <div class="form-group">
                    <label for="tgl_akhir">Tanggal Akhir</label>
                    <input type="text" id="tgl_akhir" name="tgl_akhir" class="form-control" value="09/01/2016" />
                </div>
                
                <button type="button" id="btnSubmit" class="btn btn-success" style="margin-top:24px; margin-left:1px;">Check Mutasi</button>
            </form>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <table id="dataMutasi" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>TGL TRANSAKSI</th>
                        <th>KODE CABANG</th>
                        <th>JENIS TRANSAKSI</th>
                        <th>JUMLAH TRANSAKSI</th>
                        <th>NAMA TRANSAKSI</th>
                        <th>KETERANGAN</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 id="headJsonTitle"><strong>FORMAT RESPONSE JSON</strong></h4>   
                        </div>
                        <div id="contentJson" class="panel-body"></div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>




<script src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
    window.siteurl = '<?php echo base_url(); ?>';
 
    function ConvertObjectToJSONFormat(r)
    {
        var str = "{";
        str += "<ul class='mylist'>";

        $.each(r, function(i, v){
            str += '<li>"'+i+'" : ';

            if(v.isArray || typeof v === 'object'){
                str += '{ <br>';
                for (let c = 0; c < v.length; c++) {
                    str += "<ol class='mylist'>";
                    
                    $.each(v[c], function(a, b){
                        str += '<li>"' + a + '" : "' +  b + '"</li>';
                    });

                    str += "</ol>";
                    if(c < v.length-1)
                        str += "<br>";
                }
                str += "} <br>";
            }
            else{
                str += '"' + v +'"';
            }

            str += "</li>";

        });
        str += "</ul>";
        str += "}"

        return str;
    }


    function fetch_data(toolsTable, start_date, end_date)
    {
        var dataTable = toolsTable.DataTable({
            "bProcessing" : true,
            "serverSide" : true,
            "searching" : false,
            "order":[],
            "ajax" : {
                url: window.siteurl + '/test/dev/fetch/mutasi',
                type:"POST",
                data: {
                    start_date : start_date, 
                    end_date : end_date
                }
            },
            "columnDefs": [
                { 
                    "targets": [ 0 ], 
                    "orderable": false, 
                },
            ],
        });
    }

    function viewJSON(tools, start_date, end_date) 
    {      
        $.ajax({
            url: window.siteurl + 'request/data/mutasi/bank/bca',
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify( { "start_date": start_date, "end_date" : end_date } ),
            success: function( response ){
                console.log(response);
                var strJson = ConvertObjectToJSONFormat(response.messages);
                tools.html(strJson);
            }
        });
    }

 
    $(document).ready(function(){
        var contentJSON = $("#contentJson");
        var txtTglMulai = $("#tgl_mulai");
        var txtTglAkhir = $("#tgl_akhir");

        console.log(txtTglMulai.val());

        fetch_data($('#dataMutasi'), txtTglMulai.val(), txtTglAkhir.val());
        viewJSON(contentJSON, txtTglMulai.val(), txtTglAkhir.val())
       

        $('input[name="tgl_mulai"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'),10)
        });

        $('input[name="tgl_akhir"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'), 10)
        });

        $('#btnSubmit').click(function (e) { 
            e.preventDefault();
            fetch_data($('#dataMutasi'), txtTglMulai.val(), txtTglAkhir.val());
            viewJSON(contentJSON, txtTglMulai.val(), txtTglAkhir.val())
        });

    });
</script>


</body>
</html>