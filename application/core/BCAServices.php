<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class BCAServices extends CI_Controller 
{
    private $bca_host = 'https://sandbox.bca.co.id'; 
    
    private $client_id = '4c71a8db-cc68-4de8-9f86-e67a1fb24ef7';
    private $client_secret = 'd3b9e7c3-ab1e-404d-b991-2a4f69c3b814';
	private $api_key = '44863dac-44c6-4835-a329-815aa20bca8e';
	private $api_secret = '11ff6d76-e48b-4854-a532-dea6552ed2c6'; 
	private $corporate_id = 'BCAAPI2016'; 
    private $account_number = '0201245680'; 

	private $access_token = null;
	private $signature = null;
	private $timestamp = null;
    
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /** Convert To Format JSON Decode*/
    public function JSONDecodeConverter($response)
    {
        return json_decode($response, true);
    }

    /** Generate Getter To Corporate ID */
    public function getCorporateId()
    {
        return $this->corporate_id;
    }

    /** Generate Getter To Account Number */
    public function getAccountNumber()
    {
        return $this->account_number;
    }

    public function getFullURL($path)
    {
        return $this->bca_host . $path;
    }

    
    /** Generate Setter On Getter Access Token */
    public function setAccessToken()
    {
		$fullurl = $this->getFullURL('/api/oauth/token');

		$headers = array(
			'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic '.base64_encode($this->client_id.':'.$this->client_secret)
        );
        $data = array('grant_type' => 'client_credentials');
        $response = $this->JSONDecodeConverter(BCACurlPost($fullurl, $data, $headers));
		$this->access_token = $response['access_token'];
	}

    public function getAccessToken(){
        return $this->access_token;
    }


    /** Generate Getter For Timestamp And Signature */
    private function setParseSignature($response)
    {
		$explode = explode(':', explode(',', $response)[8]);
		$this->signature = trim($explode[1]);
    }
    
    private function setParseTimestamp($response)
    {
		$explode = explode('Timestamp: ', explode(',', $response)[3]);
		$this->timestamp = trim($explode[1]);
    }
    
    private function getDefineFormatTimestamp()
    {
        $timestamp = date(DateTime::ISO8601);
		$timestamp = str_replace('+','.000+', $timestamp);
		$timestamp = substr($timestamp, 0,(strlen($timestamp) - 2));
        $timestamp .= ':00';
        return $timestamp;
    }


    /** Generate Setter Utility Signatire */
    public function setUtilitySignature($uri, $method, $data=array()){

        $fullurl = $this->getFullURL('/utilities/signature');
		$headers = array(
			'Timestamp: '.$this->getDefineFormatTimestamp(),
			'URI: '.$uri,
			'AccessToken: ' .$this->access_token,
			'APISecret: ' . $this->api_secret,
			'HTTPMethod: ' . $method,
		);

        $output = BCACurlPost($fullurl, $data, $headers);
		
		$this->setParseSignature($output);
		$this->setParseTimestamp($output);
    }

    public function getRuntimeHeader()
    {
        $runtime_header = array(
			'X-BCA-Key: '. $this->api_key,
			'X-BCA-Timestamp: '. $this->timestamp,
			'Authorization: Bearer '. $this->access_token,
			'X-BCA-Signature: '. $this->signature,
			'Content-Type: application/json',
			'Origin: '.$_SERVER['SERVER_NAME']
        );
        return $runtime_header;
    }

    public function BCAGetRequestService($path, $runtime_header)
    {
        $fullurl = $this->getFullURL($path);
        $response = BCACurlGet($fullurl, $runtime_header);
        return $this->JSONDecodeConverter($response);
    }


    

    
    
}
