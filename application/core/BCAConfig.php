<?php  defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'controllers/service_bca/BussinessBanking.php';

class BCAConfig extends CI_Controller {

    public $business_banking;

    public function __construct()
    {
        parent::__construct();
        $this->business_banking = new BussinessBanking();
    }

    /** Generate Set All Response To JSON Format */
    public function setResponse($status=null, $data=null, $optional=null, $code=200)
    {
        $array = array(
            'success' => ($status != null) ? $status : false, 
            'messages' => ($data != null) ? $data : 'No Response Data (NULL)'
        );

        if($optional != null)
            $array['optional'] = $optional;
        
        return $this->output->set_content_type('application/json')->set_status_header($code)->set_output(json_encode($array));
    }

    public function changeFormatDate($date)
    {
        return date("Y-m-d", strtotime($date));
    }

    public function setArrayMutasi($response)
    {
        $mutasi = array(
            'mulai_dari' => $response['StartDate'],
            'sampai_dengan' => $response['EndDate'],
            'mata_uang' => $response['Currency'],
            'saldo_awal' => $response['StartBalance'],
            'data_mutasi' => array()
        );

        if(is_array($response['Data']) && count($response['Data']) > 0)
        {
            foreach ($response['Data'] as $s) {
                $tmp = array(
                    'tanggal_transaksi'   => $s['TransactionDate'],
                    'kode_cabang'         => $s['BranchCode'],
                    'jenis_transaksi'     => $this->DefineTransactionType($s['TransactionType']),
                    'jumlah_transaksi'    => $s['TransactionAmount'],
                    'nama_transaksi'      => $s['TransactionName'],
                    'deskripsi_transaksi' => $s['Trailer']
                );

                array_push($mutasi['data_mutasi'], $tmp);
            }
        }

        return $mutasi;
    }

    public function DefineTransactionType($type)
    {
        switch (strtoupper($type)) {
            case 'D':
                return 'Debit';
                break;
            default:
                return 'Kredit';
                break;
        }
    }

}

/* End of file BCAConfig.php */
